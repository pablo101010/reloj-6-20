function obtenerHora() {
    let fecha = new Date();
    // console.log(fecha);
    // console.log(fecha.getDate());
    // console.log(fecha.getFullYear());
    // console.log(fecha.getDay());
    // console.log(fecha.getMonth());

    //declarar variables
    let pDiaSemana = document.getElementById("diaSemana"),
        pDia = document.getElementById("dia"),
        pAnio = document.getElementById("anio"),
        pMes = document.getElementById("mes"),
        pHoras = document.getElementById("horas"),
        pMinutos = document.getElementById("minutos"),
        pSegundos = document.getElementById("segundos"),
        pAmPm = document.getElementById("ampm");

    let dias = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

    pDiaSemana.innerText = dias[fecha.getDay()];
    pMes.innerText = meses[fecha.getMonth()];
    pDia.innerText = fecha.getDate();
    pAnio.innerText = fecha.getFullYear();

    pHoras.innerText = fecha.getHours();
    pMinutos.innerText = fecha.getMinutes();
    pSegundos.innerText = fecha.getSeconds();


    if (fecha.getSeconds() < 10) {
        pSegundos.innerText = "0" + fecha.getSeconds();
        
    } else {
        pSegundos.innerText = fecha.getSeconds();
        
    }

    if (fecha.getMinutes() < 10) {
        pMinutos.innerText = "0" + fecha.getMinutes();
    } else {
        pMinutos.innerText = fecha.getMinutes();
    }

    if (fecha.getHours() < 13) {
        pHoras.innerText = "0" + fecha.getHours();
        pAmPm.innerText= "A.M."
    } else {
        pHoras.innerText = "0" + (fecha.getHours() - 12) ;
        pAmPm.innerText= "P.M."
    }

    // //Barra de progreso
    // let barra = document.getElementById("progreso");
    // barra.style.width.= 20;
    //  console.log(pSegundos);
}

obtenerHora();
window.setInterval(obtenerHora,1000)